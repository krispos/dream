function plotRawStairs(X1, Y1, title_string)

figure1 = figure;

% Create axes
axes1 = axes('Parent',figure1,...
    'XTickLabel',['22:48';'00:00';'01:12';'02:24';'03:36';'04:48';'06:00';'07:12'],...
    'XTick',[736016.95 736017 736017.05 736017.1 736017.15 736017.2 736017.25 736017.3]);
box(axes1,'on');
hold(axes1,'all');

% Create plot
stairs(X1,Y1);

% Create xlabel
xlabel('Time');

% Create ylabel
ylabel('Acceleration');

% Create title
title(title_string);

