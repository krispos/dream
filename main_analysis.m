clear;
data_import;
sleep_phase_import;

%% ploting raw data with plotRaw
plotRaw(timestamp_acc_org,ax1,'Acceleration X-axis');
plotRaw(timestamp_acc_org,ax2,'Acceleration Y-axis');
plotRaw(timestamp_acc_org,ax3,'Acceleration Z-axis');

plotRawStairs(timestamp_sleep_org,phase_sleep_org,'Sleep phases');

%% calculating motion vector
totalAcc = sqrt(ax1.^2 + ax2.^2  + ax3.^2 );
totalAcc_diff = totalAcc(2:50902)-totalAcc(1:50901);
time_diff = timestamp_acc_org(2:50902)-timestamp_acc_org(1:50901);
motion = ((totalAcc_diff./time_diff).^2)./10^8; %./10^8 is to make signal level comparable to other signals.
figure;
plot(timestamp_acc_org(2:50902),motion);
ylim([-0.1 0.6]);
datetick('x','HH:MM','keeplimits','keepticks')
title('Motion vector');


%% oversample sleep phase
sleep_oversampled = zeros(size(motion));
prev_sleep_index = 1; %active index in sleep phase vector
for i = 1:50901
    if(prev_sleep_index<119)
        while (timestamp_sleep_org(prev_sleep_index+1)<=timestamp_acc_org(i+1))
        
            prev_sleep_index=prev_sleep_index+1;
            if(prev_sleep_index<=119)
                break;
            end
        end
    end
    sleep_oversampled(i)=phase_sleep_org(prev_sleep_index);
end
figure;
plot(timestamp_sleep_org,phase_sleep_org,timestamp_acc_org(2:50902),sleep_oversampled);
title('Oversampled sleep phases - compared to original');
datetick('x','HH:MM','keeplimits','keepticks')

%% calculating phase change vector

phase_change = [0; double(sleep_oversampled(1:50900) ~= sleep_oversampled(2:50901))];
figure;
 plot(timestamp_acc_org(2:50902),sleep_oversampled);
 hold all;
 plot(timestamp_acc_org(2:50902),phase_change);
 hold off;
title('Phase change vector');
datetick('x','HH:MM','keeplimits','keepticks')

%% noise estimation
figure;
plot(timestamp_acc_org,ax1,timestamp_acc_org,ax2,timestamp_acc_org,ax3,timestamp_acc_org(2:50902),motion);
title('acceleratios combined with motion vector');
figure;
plot(motion);
title('Motion vector - noise estimation');
axis([25500 29000 -0.001 0.006]);

threshold = 0.004; %from plot 

%% Thresholding (and ignoring magnitude of change)
motion_thresholded = double(motion>threshold);
figure;
plot(timestamp_acc_org(2:50902),motion_thresholded);
title('Thresholded motion vector');
datetick('x','HH:MM','keeplimits','keepticks');
ylim([-0.1 1.1]);

%% Binning data
nb = 150; %number of bins
bin_size = (timestamp_acc_org(50902)-timestamp_acc_org(2))/nb;
%init
motion_bins = zeros(nb,1);
phase_bins = zeros(nb,1);
motion_bins(1) =  motion_thresholded(1);
phase_bins(1) = phase_change(1);
for i = 2:50901
    where = ceil((timestamp_acc_org(i+1)-timestamp_acc_org(2))/bin_size);
    motion_bins(where) = motion_bins(where) + motion_thresholded(i);
    phase_bins(where) = phase_bins(where) + phase_change(i);
end
bar(motion_bins);
 hold all;
 plot(phase_bins,'r');
 hold off;
 xlabel('Bin');
 ylabel('Number of samples');
 legend('motion (binned)', 'sleep change (binned)');